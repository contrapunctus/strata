(ql:quickload :strata)
(in-package :strata)

;; Just a heading and some text.

(defvar *doc* (make-document))
;; the first argument to each make-* function is the version
(add-node (make-section 1 "Title") *doc*)
(add-node (make-text 1 "Some text") *doc*)
(add-layout (make-layout 1 (document-nodes *doc*)) *doc*)
;; *doc* =>
;; #<DOCUMENT NIL
;;   (#<TEXT 1 NIL NIL "Some text"> #<SECTION 1 NIL NIL "Title">)
;;   NIL
;;   (#<LAYOUT 1 NIL NIL NIL (#<SECTION 1 NIL NIL "Title">
;;                            #<TEXT 1 NIL NIL "Some text">)>)>

;; Nesting container nodes in document layouts.
(ql:quickload :trivia)
(defvar *doc* (make-document))
;; the first argument to each make-* function is the version
(add-node (make-section 1 "Section 1")   *doc*)
(add-node (make-section 1 "Section 2")   *doc*)
(add-node (make-text 1 "Some text")      *doc*)
(add-node (make-text 1 "Some more text") *doc*)
(add-layout
 (make-layout 1
              (trivia:let-match
               (((list* text2 text1 section2 section1) (document-nodes *doc*)))
               ;; Section 1 contains "Some text" followed by Section 2, which contains "Some more text"
               (list section1 text1 (list section2 text2))))
 *doc*)
;; *doc* =>
;; #<DOCUMENT NIL
;;   (#<TEXT 1 NIL NIL "Some more text"> #<TEXT 1 NIL NIL "Some text">
;;    #<SECTION 1 NIL NIL "Section 2"> #<SECTION 1 NIL NIL "Section 1">)
;;   NIL
;;   (#<LAYOUT 1 NIL NIL NIL ((#<SECTION 1 NIL NIL "Section 1">)
;;                             #<TEXT 1 NIL NIL "Some text">
;;                             (#<SECTION 1 NIL NIL "Section 2">
;;                              #<TEXT 1 NIL NIL "Some more text">))>)>
