(in-package :cl)
(defpackage :strata
  (:use :cl)
  (:export
   #:document #:make-document

   #:layer #:layout
   ;; nodes
   #:node
   #:text #:make-text

   #:image #:paragraph

   #:section #:make-section

   #:emphasis #:quote #:footnote
   #:cell #:row #:table #:link #:table-of-contents #:index #:bibliography
   #:verse #:poem #:song))
(in-package :strata)

(defclass document ()
  ((properties :initarg :properties :initform nil :accessor properties)
   (nodes      :initarg :nodes      :initform nil :accessor nodes)
   (layers     :initarg :layers     :initform nil :accessor layers)
   (layouts    :initarg :layouts    :initform nil :accessor layouts)))

(defun make-document (&key (properties nil) (nodes nil) (layers nil) (layouts nil))
  (make-instance 'document :properties properties :nodes nodes
                           :layers layers :layouts layouts))

(defmethod document-nodes ((document document))
  "Return a list of nodes from DOCUMENT."
  (with-slots (nodes) document nodes))

(defmethod print-object ((object document) stream)
  (print-unreadable-object (object stream :type t)
    (with-slots (properties nodes layers layouts) object
      (format stream "~{~s~^~%  ~}" (list properties nodes layers layouts)))))

(defclass document-component ()
  ((version    :initarg :version
               :accessor version
               :initform (error "Must have a unique version number."))
   (properties :initarg :properties
               :accessor properties
               :initform nil)
   (name       :initarg :name
               :accessor name
               :initform nil))
  (:documentation
   "Abstract - only intended for subclassing.
  Superclass of all nodes, layers, and layouts."))

(defclass node (document-component) ()
  (:documentation "Abstract - only intended for subclassing."))

(defmethod add-node ((node node) (document document))
  (push node (nodes document)))

(defgeneric node-text (node)
  (:documentation "Return direct textual content of node as a string."))

(defmethod print-object ((object node) stream)
  (print-unreadable-object (object stream :type t)
    (with-slots (version properties name content) object
      (format stream "~{~s~^ ~}" (list version properties name content)))))

(defclass text (node)
  ((content :initarg :content :accessor content))
  (:documentation "Terminal node representing a piece of textual content."))

(defun make-text (version text &optional name properties)
  (make-instance 'text :version version :content text
                       :name name :properties properties))

(defmethod node-text ((node text))
  (slot-value node 'content))

(defclass image (node)
  ((content :initarg :content :accessor content))
  (:documentation "Terminal node representing an image."))

(defclass container (node) ()
  (:documentation "Abstract superclass for all container (non-terminal) nodes in the document."))

(defclass section (container)
  ((title :initarg :title
          :accessor title
          :documentation "Title of a section. Usually displayed in a document."))
  (:documentation "General-purpose container for sectioning a document. Section levels are determined by their nesting."))

(defun make-section (version title &optional name properties)
  (make-instance 'section :version version :title title
                          :name name :properties properties))

(defmethod node-text ((node section))
  (slot-value node 'title))

(defmethod print-object ((object section) stream)
  (print-unreadable-object (object stream :type t)
    (with-slots (version properties name title) object
      (format stream "~{~s~^ ~}" (list version name properties title)))))

(defclass paragraph (container) ())

(defclass emphasis  (container) ())

(defclass quotation (container) ())

(defclass footnote  (container) ())

(defclass cell      (container) ())

(defclass row       (container) ())

(defclass table     (container) ())

(defclass link (container) ((target)))

(defclass table-of-contents (container) ())

(defclass index (container) ())

(defclass bibliography (container) ())

(defclass verse (container) ())

(defclass poem (container) ())

(defclass song (container) ())

(defclass source-block (container)
  ((language)))

(defclass layer (document-component) ()
  (:documentation "Abstract class for layers."))

(defclass style-layer (layer) ()
  (:documentation "Style information to be applied to the document."))

(defclass content-layer (layer) ()
  (:documentation "Modifications to be applied to the content of the document."))

(defclass layout (document-component)
  ((tree   :initarg :tree   :accessor tree)
   (layers :initarg :layers :initform nil :accessor layers)))

(defun make-layout (version tree &optional layers name properties)
  (make-instance 'layout :version version :tree tree :properties properties :name name :layers layers))

(defmethod add-layout ((layout strata-format:layout)
                       (document document))
  (push layout (layouts document)))

(defmethod print-object ((object layout) stream)
  (print-unreadable-object (object stream :type t)
    (with-slots (version tree name properties layers) object
      (format stream "~{~s~^ ~}" (list version name properties layers tree)))))
